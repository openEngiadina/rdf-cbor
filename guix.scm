; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(use-modules
  (guix packages)
  ((guix licenses) #:prefix license:)
  (guix download)
  (guix git-download)
  (guix build-system gnu)
  (guix build-system python)
  (gnu packages)
  (gnu packages license)
  (gnu packages build-tools)
  (gnu packages graphviz)
  (gnu packages guile-xyz)
  (gnu packages pikchr)
  (gnu packages python-web)
  (gnu packages python-xyz)
  (gnu packages python-build)
  (gnu packages xml)
  (gnu packages web)
  (gnu packages bioinformatics))

(define-public python-jinja2-2.11.3
  (package
    (inherit python-jinja2)
    (version "2.11.3")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "Jinja2" version))
       (sha256
        (base32
         "1iiklf3wns67y5lfcacxma5vxfpb7h2a67xbghs01s0avqrq9md6"))))
    (arguments `(#:tests? #f))))

(define-public python-appdirs
  (package
    (name "python-appdirs")
    (version "1.4.4")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "appdirs" version))
        (sha256
          (base32
            "0hfzmwknxqhg20aj83fx80vna74xfimg8sk18wb85fmin9kh2pbx"))))
    (build-system python-build-system)
    (home-page "https://github.com/ActiveState/appdirs")
    (synopsis
      "Determine platform-specific dirs, e.g. a \"user data dir\"")
    (description
      "This module provides a portable way of finding out where user data
should be stored on various operating systems.")
    (license license:expat)))

(define-public python-kitchen
  (package
    (name "python-kitchen")
    (version "1.2.6")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "kitchen" version))
       (sha256
        (base32
         "0g5hq2icnng9vy4www5hnr3r5srisfwp0wxw1sv5c5dxy61gak5q"))))
    (build-system python-build-system)
    (propagated-inputs
     (list python-chardet))
    (home-page "https://github.com/fedora-infra/kitchen")
    (synopsis "Python API for snippets")
    (description "@code{kitchen} module provides a python API for all sorts of
little useful snippets of code that everybody ends up writing for their projects
but never seem big enough to build an independent release.  Use kitchen and stop
cutting and pasting that code over and over.")
    (license (list license:lgpl2.1+
                   ;; subprocess.py, test_subprocess.py,
                   ;; kitchen/pycompat25/defaultdict.py:
                   license:psfl))))

(define-public python-google-i18n-address
  (package
    (name "python-google-i18n-address")
    (version "2.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "google-i18n-address" version))
        (sha256
          (base32 "1mxi5vm9gf31alpiy6dvax3497lxmr4q9mraqvqnhph1pklfdqv1"))))
    (build-system python-build-system)
    (arguments `(#:tests? #f))
    (propagated-inputs (list python-requests))
    (home-page "https://github.com/mirumee/google-i18n-address")
    (synopsis "Address validation helpers for Google's i18n address database")
    (description
      "Address validation helpers for Google's i18n address database")
    (license license:bsd-3)))

(define-public python-xml2rfc
  (package
    (name "python-xml2rfc")
    (version "3.12.4")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "xml2rfc" version))
        (sha256
          (base32 "05p3himyk4wxcdasi3m6lngzdxqw5kcgr130anl0wdkisr5wkhq9"))))
    (build-system python-build-system)
    (arguments `(#:tests? #f))
    (propagated-inputs
      (list python-appdirs
            python-configargparse
            python-google-i18n-address
            python-html5lib
            python-intervaltree
            python-jinja2-2.11.3
            python-kitchen
            python-lxml
            python-markupsafe
            python-pycountry
            python-pyflakes
            python-pyyaml
            python-requests
            python-setuptools
            python-six))
    (home-page "https://tools.ietf.org/tools/xml2rfc/trac/")
    (synopsis
      "Xml2rfc generates RFCs and IETF drafts from document source in XML according to the IETF xml2rfc v2 and v3 vocabularies.")
    (description
      "Xml2rfc generates RFCs and IETF drafts from document source in XML according to
the IETF xml2rfc v2 and v3 vocabularies.")
    (license #f)))

(package
  (name "rdf-cbor")
  (version "0.1.0")
  (source #f)
  (build-system gnu-build-system)
  (arguments '())
  (native-inputs
   (list
    pikchr
    python-xml2rfc))
  (synopsis "Specification of RDF/CBOR")
  (description #f)
  (home-page "https://codeberg.org/openEngiadina/rdf-cbor")
  (license license:gpl3+))
