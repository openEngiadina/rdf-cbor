# RDF/CBOR Serialization

This repository contains the specification of the RDF/CBOR serialization.

# Acknowledgments

RDF/CBOR was initially developed for the [openEngiadina](https://openengiadina.net) project and has been supported by the [NLnet Foundation](https://nlnet.nl/) through the [NGI0 Discovery Fund](https://nlnet.nl/discovery/).

# License

[CC-BY-SA-4.0](./LICENSES/CC-BY-SA-4.0.txt)
